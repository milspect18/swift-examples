//
//  ViewController.swift
//  Midnight Or Daytime Theme Saver
//
//  Created by Kyle Price on 5/16/18.
//  Copyright © 2018 Kyle Price. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageTopDistFromTop: NSLayoutConstraint!
    @IBOutlet weak var imageCenterOffset: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var screenView: UIView!
    @IBOutlet weak var midnightSwitch: UISwitch!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    
    private var screenWidth: CGFloat!
    private var screenHeight: CGFloat!
    
    private let animDur = 0.45
    private var sunImgIdx = 0
    private var imgLeftPos: CGFloat!
    private var imgRightPos: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let midnightModeOn = UserDefaults.standard.bool(forKey: "midnightThemeOn")
        
        screenWidth = view.frame.width
        screenHeight = view.frame.height
        
        imgLeftPos = -(screenWidth * 0.3)
        imgRightPos = screenWidth * 0.3
        
        setupUI()
        
        if midnightModeOn {
            switchUIMode(to: .Midnight)
            midnightSwitch.setOn(true, animated: true)
        }
    }
    
    
    @IBAction func switchFlipped(_ sender: Any) {
        if midnightSwitch.isOn {
            switchUIMode(to: .Midnight)
        } else {
            switchUIMode(to: .Daytime)
        }
    }
    
    
    private func setupUI() {
        imageWidth.constant = screenWidth * 0.125
        imageHeight.constant = imageWidth.constant
        
        imageView.contentMode = .scaleAspectFit
        imageCenterOffset.constant = imgLeftPos
        screenView.backgroundColor = .yellow
        
        imageView.image = UIImage(named: "sun")
    }
    
    private func switchUIMode(to: UIState) {
        switch(to) {
        case .Daytime:
            UIView.animate(withDuration: animDur, animations: {
                self.imageCenterOffset.constant = self.imgLeftPos
                self.screenView.backgroundColor = .yellow
                self.infoLabel.textColor = .black
                self.modeLabel.textColor = .black
                self.imageView.transform = self.imageView.transform.rotated(by: -0.999 * .pi)
                self.imageView.transform = self.imageView.transform.rotated(by: -0.999 * .pi)
                self.view.layoutIfNeeded()
                UserDefaults.standard.set(false, forKey: "midnightThemeOn")
            }) { (success) in
                self.flipTo(img: "sun", options: .transitionFlipFromRight)
            }
        case .Midnight:
            UIView.animate(withDuration: animDur, animations: {
                self.imageCenterOffset.constant = self.imgRightPos
                self.screenView.backgroundColor = .blue
                self.infoLabel.textColor = .white
                self.modeLabel.textColor = .white
                self.imageView.transform = self.imageView.transform.rotated(by: .pi)
                self.imageView.transform = self.imageView.transform.rotated(by: .pi)
                self.view.layoutIfNeeded()
                UserDefaults.standard.set(true, forKey: "midnightThemeOn")
            }) { (success) in
                self.flipTo(img: "moon", options: .transitionFlipFromLeft)
            }
        }
    }
    
    private func flipTo(img: String, options: UIViewAnimationOptions) {
        UIView.transition(with: imageView, duration: animDur, options: options, animations: {
            self.imageView.image = UIImage(named: img)
        }, completion: nil)
    }
}


extension ViewController {
    private enum UIState {
        case Daytime
        case Midnight
    }
}

