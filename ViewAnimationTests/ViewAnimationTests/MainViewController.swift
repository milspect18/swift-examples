//
//  MainViewController.swift
//  ViewAnimationTests
//
//  Created by Kyle Price on 6/10/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    let balloonImg: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "balloon"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Join us for fun and games!"
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let starRating: CosmosView = {
        let rating = CosmosView()
        rating.settings.emptyBorderColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
        rating.settings.emptyBorderWidth = 2
        rating.settings.emptyColor = .white
        rating.settings.filledBorderColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
        rating.settings.filledBorderWidth = 1
        rating.settings.filledColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
        rating.settings.totalStars = 5
        rating.settings.fillMode = .precise
        rating.settings.starSize = 30
        rating.translatesAutoresizingMaskIntoConstraints = false
        return rating
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(balloonImg)
        view.addSubview(descriptionTextView)
        view.addSubview(starRating)
        
        setupLayout()
    }

    
    private func setupLayout() {
        balloonImg.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        balloonImg.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        balloonImg.widthAnchor.constraint(equalToConstant: 200).isActive = true
        balloonImg.heightAnchor.constraint(equalToConstant: 225).isActive = true
        
        descriptionTextView.topAnchor.constraint(equalTo: balloonImg.bottomAnchor, constant: 100).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        starRating.topAnchor.constraint(equalTo: descriptionTextView.topAnchor, constant: 75).isActive = true
        starRating.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        starRating.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}





