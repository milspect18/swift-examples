//
//  RatingStarViewDelegate.swift
//  ViewAnimationTests
//
//  Created by Kyle Price on 6/11/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit

protocol RatingStarViewDelegate {
    func ratingStar(ratingStar: RatingStarView, willChange value: Int)
    func ratingStar(ratingStar: RatingStarView, didChange value: Int)
}
