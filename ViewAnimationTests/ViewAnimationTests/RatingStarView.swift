//
//  RatingStarView.swift
//  ViewAnimationTests
//
//  Created by Kyle Price on 6/11/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit

@IBDesignable class RatingStarView: UIStackView {
    var numStars: Int = 5 {
        didSet {
            for x in 0..<starButtons.count {
                removeArrangedSubview(starButtons[x])
            }
            setupButtons()
        }
    }
    
    var unfilledImg = UIImage() {
        willSet {
            for x in 0..<starButtons.count {
                starButtons[x].setBackgroundImage(newValue, for: .normal)
            }
        }
    }
    
    var filledImg = UIImage() {
        willSet {
            for x in 0..<starButtons.count {
                starButtons[x].setBackgroundImage(newValue, for: .selected)
            }
        }
    }
    
    var rating: Int = 0 {
        willSet {
            if let delegate = delegate {
                delegate.ratingStar(ratingStar: self, willChange: newValue)
            }
        }
        didSet {
            for x in 0..<rating {
                starButtons[x].setBackgroundImage(filledImg, for: .normal)
            }
            
            for x in rating..<numStars {
                starButtons[x].setBackgroundImage(unfilledImg, for: .normal)
            }
            
            if let delegate = delegate {
                delegate.ratingStar(ratingStar: self, didChange: rating)
            }
        }
    }
    
    var starButtons = [UIButton]()
    var delegate: RatingStarViewDelegate!
    
    
    // MARK: Initializations
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButtons()
    }
    
    
    // MARK: Button actions
    @objc private func ratingButtonPressed(button: UIButton) {
        if let selected = starButtons.index(of: button) {
            rating = selected + 1
        }
    }
    
    
    // MARK: Private methods
    private func setupButtons() {
        for _ in 0..<numStars {
            let button = UIButton()
            
            button.setBackgroundImage(unfilledImg, for: .normal)
            button.setBackgroundImage(filledImg, for: .selected)
            
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: 44).isActive = true
            button.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            button.addTarget(self, action: #selector(ratingButtonPressed(button:)), for: .touchUpInside)
            
            addArrangedSubview(button)
        
            starButtons.append(button)
        }
    }
}
