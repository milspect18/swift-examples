//
//  PlaySoundsViewController.swift
//  Pick Your Pitch
//
//  Created by Udacity on 1/5/15.
//  Copyright (c) 2014 Udacity. All rights reserved.
//
//  This project shows the use of the UserDefaults storage location
//  for persisting the value of the audio playback pitch slider.
//
//  The application presents a screen with a microphone button,
//  when pressed the application will begin recording audio and the
//  microphone button will be changed to a stop button.  After the user
//  presses the stop button, the playback view controller is pushed into
//  the nav controller.  Here there is a play button for playing the
//  audio back, as well as a slider for selecting a pitch for playback.
//
//  The code written by Kyle Price is the code related to persisting
//  the pitch slider value.  The simplest way to do this would have been
//  to simply save the slider value for recall.  The approach I took was
//  converting the value to a percentage of overall slider range and save
//  that percentage.  This method has the advantage of being impervious to
//  slider range change through app update.  Though this may not be likely
//  it maintains the appropriate location even if the min and max values
//  of the slider are modified as the application is developed.

import UIKit
import AVFoundation

// MARK: - PlaySoundsViewController: UIViewController

class PlaySoundsViewController: UIViewController {

    // MARK: Properties
    
    let SliderValueKey = "sliderPercentage"
    var audioPlayer:AVAudioPlayer!
    var receivedAudio:RecordedAudio!
    var audioEngine:AVAudioEngine!
    var audioFile:AVAudioFile!
    
    // MARK: Outlets
    
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: receivedAudio.filePathUrl as URL)
        } catch _ {
            audioPlayer = nil
        }
        audioPlayer.enableRate = true   // This will crash if the do throws...

        audioEngine = AVAudioEngine()
        do {
            audioFile = try AVAudioFile(forReading: receivedAudio.filePathUrl as URL)
        } catch _ {
            audioFile = nil
        }
        
        setUserInterfaceToIsPlaying(false)
        recallPreviousSliderPosition()
    }
    
    // MARK: Set Interface
    
    func setUserInterfaceToIsPlaying(_ isPlayMode: Bool) {
        startButton.isHidden = isPlayMode
        stopButton.isHidden = !isPlayMode
        sliderView.isEnabled = !isPlayMode
    }

    // MARK: Actions
    
    @IBAction func playAudio(_ sender: UIButton) {
        
        // Get the pitch from the slider
        let pitch = sliderView.value
        
        // Play the sound
        playAudioWithVariablePitch(pitch)
        
        // Set the UI
        setUserInterfaceToIsPlaying(true)
        
    }
    
    @IBAction func stopAudio(_ sender: UIButton) {
        audioPlayer.stop()
        audioEngine.stop()
        audioEngine.reset()
    }
    
    @IBAction func sliderDidMove(_ sender: UISlider) {
        let fullWidth = sliderView.maximumValue - sliderView.minimumValue
        let sliderPosOffset = sender.value - sender.minimumValue
        let sliderNewPercent = (sliderPosOffset / fullWidth) * 100
        UserDefaults.standard.set(sliderNewPercent, forKey: SliderValueKey)
    }
    
    // MARK: Play Audio
    
    func playAudioWithVariablePitch(_ pitch: Float){
        audioPlayer.stop()
        audioEngine.stop()
        audioEngine.reset()
        
        let audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)
        
        let changePitchEffect = AVAudioUnitTimePitch()
        changePitchEffect.pitch = pitch
        audioEngine.attach(changePitchEffect)
        
        audioEngine.connect(audioPlayerNode, to: changePitchEffect, format: nil)
        audioEngine.connect(changePitchEffect, to: audioEngine.outputNode, format: nil)
        
        audioPlayerNode.scheduleFile(audioFile, at: nil) {
            // When the audio completes, set the user interface on the main thread
            DispatchQueue.main.async {self.setUserInterfaceToIsPlaying(false) }
        }
        
        do {
            try audioEngine.start()
        } catch _ {
        }
        
        audioPlayerNode.play()
    }
    
    // MARK: Persistence - recall value
    
    private func recallPreviousSliderPosition() {
        let sliderPercentage = UserDefaults.standard.float(forKey: SliderValueKey)
        let fullWidth = sliderView.maximumValue - sliderView.minimumValue
        let sliderPosOffset = (sliderPercentage / 100.0) * fullWidth
        
        sliderView.value = sliderView.minimumValue + sliderPosOffset
        
        print("Max: \(sliderView.maximumValue), Saved Percent: \(sliderPercentage), New Slider Pos: \(sliderView.value)")
    }
}
