//
//  DataController.swift
//  Mooskine
//
//  Created by Kyle Price on 5/30/18.
//  Copyright © 2018 Udacity. All rights reserved.
//

import Foundation
import CoreData

class DataController {
    let persistentContainer: NSPersistentContainer
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    init(modelName: String) {
        persistentContainer = NSPersistentContainer(name: modelName)
    }
    
    func load(completion: (() -> Void)? = nil) {
        persistentContainer.loadPersistentStores { (description, error) in
            guard error == nil else {
                fatalError(error!.localizedDescription)
            }
            
            self.autoSaveContext()
            completion?()
        }
    }
}


extension DataController {
    func autoSaveContext(interval: TimeInterval = 30) {
        print("Saving...")
        
        guard interval > 0 else {
            print("The autosave time interval must be positive!")
            return
        }
        
        if viewContext.hasChanges {
            try? viewContext.save()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
            self.autoSaveContext(interval: interval)
        }
    }
}



