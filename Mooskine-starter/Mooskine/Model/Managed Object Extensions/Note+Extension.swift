//
//  Note+Extension.swift
//  Mooskine
//
//  Created by Kyle Price on 5/31/18.
//  Copyright © 2018 Udacity. All rights reserved.
//

import Foundation
import CoreData

extension Note {
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        creationDate = Date()
        attributedText = NSAttributedString(string: "New Note...")
    }
}
